# SeaNet_PyTorch
### This repository is an official PyTorch implementation of the paper "Soft-edge Assisted Network for Single Image Super-Resolution" (in review).


The task of single image super-resolution (SISR) is a highly ill-posed inverse problem since the reconstructing of high-frequency details from a low-resolution image is challenging. Most previous CNN-based super-resolution (SR) methods tend to learn the mapping between low-resolution (LR) and high-resolution (HR) images directly through complex convolutional neural networks. However, it is not optimal to increase the depth of the network blindly, since it only improves the performance slightly. A more efficient method is integrating the image prior knowledge into the model to assist image reconstruction. In this paper, we propose a Soft-edge Assisted Network (SeaNet) to reconstruct high-quality SR images with the assistance of image soft-edge. The proposed SeaNet consists of three sub-nets: rough image reconstruction network (RIRN), soft-edge reconstruction network (Edge-Net) and image refinement network (IRN). The model first reconstructs the rough SR feature maps and the soft-edge by RIRN and Edge-Net respectively. Then, we sent the rough SR feature maps and the soft-edge to the image refinement network (IRN) to reconstruct SR images with high-frequency details. Extensive experiments show that our SeaNet converges quickly and achieves excellent performance with the help of the soft-edge prior.


### SEAN is another name for SeaNet, which is convenient for us to conduct experiments.

## Prerequisites:
1. Python 3.6
2. PyTorch >= 0.4.0
3. numpy
4. skimage
5. imageio
6. matplotlib
7. tqdm

For more informaiton, please refer to <a href="https://github.com/thstkdgus35/EDSR-PyTorch">EDSR</a> and <a href="https://github.com/yulunzhang/RCAN">RCAN</a>.

## Document
Train/             : all train files are stored here

Test/              : all test files are stored here

README.md : read me first

demo.sh        : all running instructions


## Dataset

We use DIV2K dataset to train our model. Please download it from <a href="https://data.vision.ee.ethz.ch/cvl/DIV2K/">here</a>  or  <a href="https://cv.snu.ac.kr/research/EDSR/DIV2K.tar">SNU_CVLab</a>.

Extract the file and put it into the Train/dataset.

Only DIV2K is used as the training dataset, and Flickr2K is not used as the training dataset !!!

We use generate_edge.m to extract the soft-edge of DIV2K and put them into the Train/dataset/DIV2K/DIV2K_train_EDGE


## Training

Using --ext sep_reset argument on your first running. 

You can skip the decoding part and use saved binaries with --ext sep argument in second time.

```
cd Train/
# SEAN x2  LR: 48 * 48  HR: 96 * 96
python main.py --template SEAN --save SEAN_X2 --scale 2 --reset --save_results --patch_size 64 --ext sep_reset

# SEAN x3  LR: 48 * 48  HR: 144 * 144
python main.py --template SEAN --save SEAN_X3 --scale 3 --reset --save_results --patch_size 96 --ext sep_reset

# SEAN x4  LR: 48 * 48  HR: 192 * 192
python main.py --template SEAN --save SEAN_X4 --scale 4 --reset --save_results --patch_size 128 --ext sep_reset

```

## Testing

All original test datasets (HR images) can be downloaded from <a href="https://www.jianguoyun.com/p/DaSU0L4Q19ySBxi_qJAB">here</a>.

Different from previous works to select the best weight as the final model weight, we use the weight of the last epoch as our final model weight directly.

Using pre-trained model for training, all test datasets must be pretreatment by  Prepare_TestData_HR_LR.m and all pre-trained model should be put into Test/model/ first.

```
#SEAN x2
python main.py --data_test MyImage --scale 2 --model SEAN --pre_train ../model/SEAN_x2.pt --test_only --save_results --chop --save "SEAN" --testpath ../LR/LRBI --testset Set5

#SEAN+ x2
python main.py --data_test MyImage --scale 2 --model SEAN --pre_train ../model/SEAN_x2.pt --test_only --save_results --chop --self_ensemble --save "SEAN_plus" --testpath ../LR/LRBI --testset Set5


#SEAN x3
python main.py --data_test MyImage --scale 3 --model SEAN --pre_train ../model/SEAN_x3.pt --test_only --save_results --chop --save "SEAN" --testpath ../LR/LRBI --testset Set5

#SEAN+ x3
python main.py --data_test MyImage --scale 3 --model SEAN --pre_train ../model/SEAN_x3.pt --test_only --save_results --chop --self_ensemble --save "SEAN_plus" --testpath ../LR/LRBI --testset Set5


#SEAN x4
python main.py --data_test MyImage --scale 4 --model SEAN --pre_train ../model/SEAN_x4.pt --test_only --save_results --chop --save "SEAN" --testpath ../LR/LRBI --testset Set5

#SEAN+ x4
python main.py --data_test MyImage --scale 4 --model SEAN --pre_train ../model/SEAN_x4.pt --test_only --save_results --chop --self_ensemble --save "SEAN_plus" --testpath ../LR/LRBI --testset Set5
```

We also introduce self-ensemble strategy to improve our SEAN and denote the self-ensembled version as SEAN+.

More running instructions can be found in demo.sh.


## Performance

We use  Test/PSNR_SSIM_Results_BI_model.txt for PSRN/SSIM test.

<p align="center">
<img src="images/Results1.png" width="800px"/> 
</p>

<p align="center">
<img src="images/Results2.png" width="800px"/> 
</p>


## Convergence Analyses

SEAN x2 x3 x4 on DIV2K training dataset.

<p align="center">
<img src="images/loss_L1_x2.png" width="250px"/><img src="images/loss_L1_x3.png" width="250px" /> <img src="images/loss_L1_x4.png" width="250px"/> 
</p>



## Results

<p align="center">
<img src="images/SR.png" width="800px"/> 
</p>

<p align="center">
<img src="images/EDGE.png" width="800px"/> 
</p>


More reconstructed SR images can be downloaded  <a href="http://t.cn/EUnPgu6">SeaNet_SR.zip</a>


We will publish the paper and authors information after the paper is accepted.

More detailed ablation analysis experiments will be publish after the paper is accepted.
